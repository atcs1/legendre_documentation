Introduction
===================

## :thinking: Rationale

<span style="background-color: lightblue; padding: 1em; display: inline-block; width: fit-content; min-width: 100px; min-height: 50px">
The activities of Product Discovery (PD) and Target Discovery (TD)
often require in-silico validation of prospective target genes for new
product development. Currently, these validations are carried out in
an inconsistent and project-specific manner, despite the fact that
many of these processes could be standardized. Our project introduces
a target ranking approach, offering a uniform yet adaptable method for
in-silico target validation.
</span>

Acquiring a list of target genes during a PD/TD project often includes
in-silico target expression evaluation. This is typically conducted
using single cell or bulk expression data (see
<https://genmab.atlassian.net/l/cp/2qyQ44xn>). It\'s common to compare
the gene expression of cancer cells to healthy cells during a
Differential Expressed Gene (DEG) analysis. This process yields
additional data useful for target validation. Currently, the
validation process relies on setting thresholds on various gene
attributes, such as gene expression levels, log-fold changes of gene
expressions between cancer and healthy cells, and the percentage of
cells expressing the target gene within the target cell population.
These potential target genes are then evaluated against gene
expression distributions from [The Cancer Genome Atlas
(TCGA)](https://www.cancer.gov/ccg/research/genome-sequencing/tcga)
and [Genotype-Tissue Expression
(GTEx)](https://www.gtexportal.org/home/) data.

The existing approach is not uniform and working with thresholds has its
shortcomings. Setting optimal thresholds is challenging, and rigid
cutoffs often lack justification. For instance, it\'s difficult to
convincingly argue why genes with an adjusted p-value of 0.05 should be
retained, while those with 0.0501 should be removed, especially when the
latter might have a superior safety profile.

Our proposed solution is to use a ranking approach. This approach avoids
relying on rigid cut-off values, instead ordering the genes based on
their relative specificity, safety, and sink profile. This not only
enhances the accuracy of the process but also allows for more nuanced
analyses.

## :office_worker: Description of the ranking approach

!!! info

	This is a short overview section on the ranking approach. 
	More information at [Description of the ranking approach](description_of_target_ranking.md)

<figure>
    <img src="/img/image-20230727-093233.png"
         alt="legendRe logo">
    <figcaption>Overview of the ranking approach</figcaption>
</figure>


The ranking approach takes a set of potential target genes and performs
ranking of these genes based on Specificity, Safety and Sink scoring.

### ![](/img/spec.png){ align=left; width="25" } Specificity

Validate the target gene expression in target cell population or tissue.
This score is based on expression in target cell population (single cell
RNA expression), expression in target tissues and indications (bulk RNA
expression) and DEG score between target and non-target population.

### ![](/img/safety.png){ align=left; width="25" } Safety

Validate target gene expression in normal cells and tissues (single cell
RNA and bulk RNA expression).

### ![](/img/sink.png){ align=left; width="25" } Sink

Validate target gene expression in granulocytes, platelets, monocytes,
neutrophils, erythrocytes (single cell RNA expression).

### ![](/img/rank.png){ align=left; width="25" } Final ranking

The final target score is the combination of these scores that provides
the final ranking.

!!! info

	This framework is not limited to Specificity, Safety and Sink scoring
	but can be extended and customized. For example, within each score the
	weights of the different variables can be changed as well as new data
	can be used to define additional variables. It is also possible to
	define new scoring profiles beside the proposed ones. For more
	information, see the tutorial on the software implementation [here](target_scoring_tutorial_db.html) and [here](target_scoring_tutorial.md).

## :mechanic: Implementation of the ranking approach

<figure>
    <img src="/img/DAlle2_v1.jpg" width="200" height="100"
         alt="legendRe logo">
</figure>

The ranking approach is implemented in the LegendRe software package
(named after <https://en.wikipedia.org/wiki/Adrien-Marie_Legendre>).
LegendRe stands for Linear Gene Data Ranker, which is an R package that
provides the functionalities of our target ranking approach.

LegendRe is modular and extensible. While the target ranking process can
be run as a whole, it is also possible run only specific parts of the
pipeline. It is also possible to add new data sources, modify existing
and create new scoring approaches, define reference genes as baselines
for scoring, and much more.

Installation and usage:

You can install the package [via
GitLab](https://gitlab.com/genmab/production/non-gxp/data-science/tdpd-data-science/poc/Attila_Csala/legendre)
or [via
RSPM](https://datascience.genmab.net/rstudio-pm/client/#/repos/3/packages/LEGENDRE)
in your own environment or use the package on the
"[Ranking_cluster](https://genmab-prod.cloud.databricks.com/?o=6691901644552278#setting/clusters/0706-144207-xionex4c/configuration)"
on Databricks. Follow the tutorial for more information on installing
and starting up with the package.

-   [Tutorial on target scoring with LegendRe](target_scoring_tutorial.md)

-   [LegendRe · GitLab (genmab.net)](https://datascience.genmab.net/gitlab/atcs/legendre)

-   [Cluster Details - Databricks](https://genmab-prod.cloud.databricks.com/?o=6691901644552278#setting/clusters/0706-144207-xionex4c/configuration)

## :truck: Deliverables

-   Code that enables the functionalities described for the ranking
    approach.

    -   inputs potential target genes and returns a ranked list of genes

    -   enables Specificity, Safety and Sink scoring

-   Documentation of the method

    -   Describe how the method works, what is the required and optional
        input data, what is the output data, interpretation of the
        results

    -   Provide a tutorial

-   Provide a package

-   Set up compute environment to run the package

## :busts_in_silhouette: Stakeholders

  -----------------------------------------------------------------------
  **Name**                                          | **Role**
  --------------------------------------------------| --------------------
  Attila Csala (ATCS)                               | Project owner
  Chris Klijn (CKL)                                 | Project initiator
  TD and PD Scientists and Data Scientists          | Potential users

## :man_surfing: Applications

### [PDAC Product discovery project by TAAM slides](https://genmab.atlassian.net/wiki/download/attachments/443744413/230627%20PDAC%20targets%20based%20on%20MACE%20annotation_taab.pptx?api=v2)

### [CRC Target discovery project by FEVI slides](https://genmab.atlassian.net/wiki/download/attachments/443744413/230906+DDS+Team+meeting+Update+on+Colorectal+Cancer+Target+Identification.pptx?version=1)

### [CAF Target discovery project by ATCS slides](https://genmab.atlassian.net/wiki/download/attachments/443744413/230711_CAF_LRRC15_target_concept_meeting_ATCS.pptx?version=1)

## :books: References

-   <https://genmab.atlassian.net/l/cp/2qyQ44xn>

-   [The Human Protein Atlas](https://www.proteinatlas.org/)

-   [Tabula Sapiens
    (czbiohub.org)](https://tabula-sapiens-portal.ds.czbiohub.org/)

-   <https://www.sc-best-practices.org/preamble.html>

-   <https://bioconductor.org/books/release/OSCA/>

## :scroll: Logs

`26 Jan 2024`

Updated the package from 0.1.0 to 0.1.1, the new version is compatible
with R 4.2.2. As of now it is available via Gitlab and requested an IT
ticket to push the new version to RSPM as well.

You can also install the package from source via
`install.packages("/dbfs/mnt/gmb-ds-discovery/projects/legendre/LegendRe_0.1.1.tar.gz")`

`13 Nov 2023`

Following popular demand, changed the package logo.

<figure>
    <img src="/img/legendre_logo3.png" width="200" height="100"
         alt="legendRe old logo">
    <figcaption>Old logo using Legendre's Wikipedia picture</figcaption>
</figure>

<figure>
    <img src="/img/DAlle2_v1.jpg" width="200" height="100"
         alt="legendRe logo">
    <figcaption>New logo made by chatGPT</figcaption>
</figure>

`20 Sep 2023`

Shared target ranking application slides of PDAC, CRC and CAF projects

`18 Aug 2023`

The package is available via RSPM and can be installed via

`install.packages(pkgs="LegendRe", type="source", repos="https://datascience.genmab.net/rstudio-pm/DataScience/latest")`

`31 Jul 2023`

Documentation is released on Confluence

`27 Jul 2023`

Finished the implementation, the R package is available via [Attila
Csala / LegendRe · GitLab
(genmab.net)](https://datascience.genmab.net/gitlab/atcs/legendre)

Named the package after Legendre given he is attributed to the discovery
of the least squares method
<https://en.wikipedia.org/wiki/Adrien-Marie_Legendre#Mathematical_work>

`29 Jun 2023`

Consulted ChatGPT-4 for catchy names...


<figure>
    <img src="/img/image-20230727-153310.png"
         alt="legendRe logo">
</figure>

`27 Jun 2023`

The deliverable for the product of this project are:

-   Code that enables the functionalities described for the ranking
    approach.

    -   inputs potential target genes and returns a ranked list of genes

    -   enables Specificity, Safety and Sink scoring

-   Documentation of the method

    -   Describe how the method works, what is the required and optional
        input data, what is the output data, interpretation of the
        results

    -   Provide a tutorial

-   Provide a package

-   Set up compute environment to run the package

`27 Jun 2023`

Initial idea for this project to develop an approach on ranking target
genes based on specificity, safety and sink scoring profiles came up
during a meeting in April 2023. The idea of ranking came as a proposed
solutions for the following problems:

-   Obtaining a list of target genes from a "typical" target discovery
    analysis that includes differential gene expression (DEG) analysis
    and safety profiling. This process requires arbitrary cut-off
    thresholds, e.g.; threshold on

    -   log-fold change,

    -   adjusted p-value,

    -   scRNA-seq expression in target cell population,

    -   percentage of cells that expressing the target gene in target
        cell population,

    -   bulk RNA-seq expression in cancer and in healthy tissue

-   There is no optimal way to set thresholds for these values and
    stringent cut-offs often doesn't make sense (e.g.; hard to give
    clear motivation why retain genes with adjusted p-value of 0.05 but
    remove the ones with 0.050001 while the latter one might have had a
    much better safety profile).

-   Using ranking instead allows one to avoid working with hard cut-off
    values and instead orders the genes based on their relative
    specificity, safety and sink profile among each other.
