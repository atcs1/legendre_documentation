Description of the target ranking approach
===============


<figure>
    <img src="/img/image-20230727-093233.png"
         alt="legendRe logo">
    <figcaption>Overview of the ranking approach</figcaption>
</figure>


The ranking approach takes a set of potential target genes and
performs ranking of these genes based on Specificity, Safety and Sink
scoring.

---

## ![](img/spec.png){ align=left; width="40" } Specificity

Validate the target gene expression in target cell population or tissue.
This score is based on expression in target cell population (single cell
RNA expression), expression in target tissues and indications (bulk RNA
expression) and DEG score between target and non-target population.

>
> ![](img/data.png){ align=left; width="20" }  Data used to calculate the specificity score:
> 
> - single cell RNA expression data on target population,
> - average log fold change between target vs non-target population from the DEG analysis
> - adjusted p-value from the DEG analysis
> - percentage of target population cells expressing target vs percentage of non-target population cells expressing target,
> - TCGA bulk RNA expression as validation of expression in target indication(s)
>

The Specificity score is computed as a linear combination of these variables, i.e.;

<center>
![](img/spec.png){ align=left; width="20" } Specificity = 1 x (scRNA target expression) + 1 x (logFC) - 1 x (adj_p_val) + 1 x (pct_expression) + 1 x (median_TCGA)
</center>

This score normalized between 0 and 1. A high Specificity score means; high expression in target cell population, high average log fold change between target and non target population, low adjusted p-value, high percentage of cells in target population expressing the given target, and high median bulk RNA expression over the different indications measured in TCGA - compared to other potential target genes in the analysis. A higher Specificity score is better.

---

## ![](img/safety.png){ align=left; width="40" } Safety

Validate target gene expression in normal cells and tissues (single cell RNA and bulk RNA expression)

> 
> ![](img/data.png){ align=left; width="20" } Data used to calculate the safety score:
> 
> - single cell RNA expression on normal cells
> - Tabula Sapiens and Human Protein Atlas single cell RNA expression on normal cells
> - GTEX tissue bulk RNA expression
>

The Safety score is computed as a linear combination of these variables, i.e.;

<center>
![](img/safety.png){ align=left; width="20" } Safety = -1 x (scRNA normal expression) - 1 x  (TS scRNA expression) - 1 x (HPA scRNA expression) - 1 x (GTEX bulk RNA expression)
</center>

This score normalized between 0 and 1. A high Safety score means; low expression in normal cell population, low TS and HPA single cell RNA expression and low median bulk RNA expression over different GTEX tissues - compared to other potential target genes in the analysis.

---

## ![](img/sink.png){ align=left; width="40" } Sink

Validate target gene expression in granulocytes, platelets, monocytes,
neutrophils, erythrocytes (single cell RNA expression)

> 
> ![](img/data.png){ align=left; width="20" } Data used to calculate the safety score:
> 
> - Tabula Sapiens single cell expression data on granulocytes, platelets, monocytes, neutrophils and erythrocytes
> 

The Sink score is computed as a linear combination of these variables, i.e.;

<center>
![](img/sink.png){ align=left; width="20" } Sink = 1 x (scRNA granulocytes expression) + 1 x (scRNA platelets expression) + 1 x (scRNA monocytes expression) + 1 x (scRNA neutrophils expression) + 1 x (scRNA erythrocytes expression)
</center>

This score normalized between 0 and 1. A high Sink score means; high expression in granulocytes, platelets, monocytes, neutrophils and erythrocytes - compared to other potential target genes in the analysis.

---

## ![](img/rank.png){ align=left; width="40" } Final ranking

The final target score is the combination of these scores that provides the final ranking, i.e.;

<center>
![](img/rank.png){ align=left; width="20" } Final ranking score =  
1 x ![](img/spec.png){ align=left; width="20" } + 1 x ![](img/safety.png){ align=left; width="20" } + 1 x ![](img/sink.png){ align=left; width="20" }
</center>

This score normalized between 0 and 1. A high Final ranking score means better Specificity, Safety, and Sink profile.

!!! info

	You probably guessed by now that this framework is not limited to Specificity, Safety and Sink scoring but can be extended and customized. For example, within each score the weights of the different variables can be changed as well as new data can be used to define additional variables. It is also possible to define new scoring profiles beside the proposed ones. For more information, see the tutorial on the software implementation [here](target_scoring_tutorial_db.html) and [here](target_scoring_tutorial.md).
