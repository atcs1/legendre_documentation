Generating the docs
----------

Use [mkdocs](http://www.mkdocs.org/) structure to update the documentation. 

Build locally with:

    mkdocs build

Serve locally with:

    mkdocs serve

Deplying and accessing documentation

Information on deplying mkdocs on gitlab can be found here https://squidfunk.github.io/mkdocs-material/publishing-your-site/#gitlab-pages-material-for-mkdocs

Live docs are served at https://atcs1.gitlab.io/legendre_documentation